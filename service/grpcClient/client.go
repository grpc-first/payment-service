package grpcclient

import (
	"fmt"

	"gitlab.com/grpc-first/payment-service/config"
	ss "gitlab.com/grpc-first/payment-service/genproto/store"
	us "gitlab.com/grpc-first/payment-service/genproto/user"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type IServiceManager interface {
	StoreService() ss.StoreServiceClient
	UserService() us.UserServiceClient
}

type ServiceManager struct {
	Config       config.Config
	Storeservice ss.StoreServiceClient
	Userservice  us.UserServiceClient
}

func New(c config.Config) (IServiceManager, error) {
	storeConnection, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.StoreServiceHost, c.StoreServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("ERROR WHILE CONNECTING TO STORE SERVICE ON HOST: %s PORT: %d",
			c.StoreServiceHost, c.StoreServicePort)
	}

	userConnection, err := grpc.Dial(
		fmt.Sprintf("%s:%d", c.UserServiceHost, c.UserServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, fmt.Errorf("ERROR WHILE CONNECTING TO USERSERVICE ON HOST: %s PORT: %d",
			c.UserServiceHost, c.UserServicePort)
	}
	serviceManager := &ServiceManager{
		Config:       c,
		Storeservice: ss.NewStoreServiceClient(storeConnection),
		Userservice:  us.NewUserServiceClient(userConnection),
	}

	return serviceManager, nil
}

func (s *ServiceManager) StoreService() ss.StoreServiceClient {
	return s.Storeservice
}

func (s *ServiceManager) UserService() us.UserServiceClient {
	return s.Userservice
}
