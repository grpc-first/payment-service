package service

import (
	"context"

	"github.com/jmoiron/sqlx"
	ps "gitlab.com/grpc-first/payment-service/genproto/payment"
	us "gitlab.com/grpc-first/payment-service/genproto/user"
	"gitlab.com/grpc-first/payment-service/pkg/logger"
	grpcclient "gitlab.com/grpc-first/payment-service/service/grpcClient"
	"gitlab.com/grpc-first/payment-service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type PaymentService struct {
	Client  grpcclient.IServiceManager
	Storage storage.IsStorage
	Logger  logger.Logger
}

func NewPaymentService(client grpcclient.IServiceManager, db *sqlx.DB, l logger.Logger) *PaymentService {
	return &PaymentService{
		Client:  client,
		Storage: storage.NewStoragePg(db),
		Logger:  l,
	}
}

func (s *PaymentService) CreateCard(ctx context.Context, req *ps.CreateCardRequest) (*ps.Card, error) {
	_, err := s.Client.UserService().GetUsersById(ctx, &us.Ids{Ids: []int64{req.OwnerId}})
	if err != nil {
		s.Logger.Error("Erorr while getting user info to user: id", logger.Any("create", err))
		return &ps.Card{}, status.Error(codes.Internal, "Check you data correctness")
	}
	res, err := s.Storage.Store().CreateCard(req)

	if err != nil {
		s.Logger.Error("Erorr while Creating new card to user: id", logger.Any("create", err))
		return nil, status.Error(codes.Internal, "Check you data correctness")
	}
	return res, nil
}

func (s *PaymentService) SubtractBalance(ctx context.Context, req *ps.UpdateBalanceRequest) (*ps.Card, error) {
	card, err := s.Storage.Store().SubtractBalance(req)
	if err != nil {
		s.Logger.Error("Erorr while subtracting from card", logger.Any("update", err))
		return nil, status.Error(codes.InvalidArgument, "Check your balance first")
	}
	return card, nil
}

func (s *PaymentService) AddBalance(ctx context.Context, req *ps.UpdateBalanceRequest) (*ps.Card, error) {
	card, err := s.Storage.Store().AddBalance(req)
	if err != nil {
		s.Logger.Error("Erorr while adding to balance", logger.Any("update", err))
		return nil, status.Error(codes.InvalidArgument, "Please check your data correctness")
	}
	return card, nil
}

func (s *PaymentService) GetCardInfo(ctx context.Context, req *ps.Id) (*ps.Card, error) {
	card, err := s.Storage.Store().GetCardInfo(req)
	if err != nil {
		s.Logger.Error("Erorr while getting card info", logger.Any("get", err))
		return nil, status.Error(codes.InvalidArgument, "Please check your data correctness")
	}
	return card, nil
}

func (s *PaymentService) DeleteCard(ctx context.Context, req *ps.Id) (*ps.Empty, error) {
	err := s.Storage.Store().DeleteCard(req)
	if err != nil {
		s.Logger.Error("Error while deleting card info", logger.Any("Delete", err))
		return nil, status.Error(codes.Internal, "Couldn't Delete")
	}
	return nil, nil
}
