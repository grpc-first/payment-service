package postgres

import (
	"fmt"

	"github.com/bxcodec/faker/v4"
	"github.com/jmoiron/sqlx"
	ps "gitlab.com/grpc-first/payment-service/genproto/payment"
)

type PaymentRepo struct {
	Db *sqlx.DB
}

func NewPatmentRepo(db *sqlx.DB) *PaymentRepo {
	return &PaymentRepo{
		Db: db,
	}
}

func (r *PaymentRepo) CreateCard(req *ps.CreateCardRequest) (*ps.Card, error) {
	response := &ps.Card{}
	ccNumver := faker.CCNumber()
	fmt.Println(ccNumver)
	err := r.Db.QueryRow(`INSERT INTO cards(
		card_number, 
		card_type, 
		owner_id, 
		balance) 
		values($1, $2, $3, $4) returning *`,
		ccNumver, req.Type, req.OwnerId, 100,
	).Scan(
		&response.Id,
		&response.Number,
		&response.Type,
		&response.OwnerId,
		&response.Balance,
	)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (r *PaymentRepo) SubtractBalance(req *ps.UpdateBalanceRequest) (*ps.Card, error) {
	response := &ps.Card{}
	err := r.Db.QueryRow(`update cards SET balance=balance-$1 where owner_id = $2 
	returning 
	id,
	card_number, 
	card_type, 
	owner_id, 
	balance`,
		req.Balance, req.OwnerId).Scan(
		&response.Id,
		&response.Number,
		&response.Type,
		&response.OwnerId,
		&response.Balance,
	)

	if err != nil {
		return nil, err
	}
	return response, nil
}

func (r *PaymentRepo) AddBalance(req *ps.UpdateBalanceRequest) (*ps.Card, error) {
	response := &ps.Card{}
	err := r.Db.QueryRow(`update cards SET balance=(balance+$1) where owner_id = $2 
	returning 
	id,
	card_number, 
	card_type, 
	owner_id, 
	balance`,
		req.Balance, req.OwnerId).Scan(
		&response.Id,
		&response.Number,
		&response.Type,
		&response.OwnerId,
		&response.Balance,
	)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (r *PaymentRepo) GetCardInfo(req *ps.Id) (*ps.Card, error) {
	response := &ps.Card{}
	err := r.Db.QueryRow(`select id,
	card_number, 
	card_type, 
	owner_id, 
	balance
	from cards where owner_id = $1`, req.OwnerId).Scan(
		&response.Id,
		&response.Number,
		&response.Type,
		&response.OwnerId,
		&response.Balance,
	)
	return response, err
}

func (r *PaymentRepo) DeleteCard(req *ps.Id) error {
	_, err := r.Db.Exec(`delete from cards where owner_id=$1`, req.OwnerId)
	return err
}
