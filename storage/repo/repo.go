package repo

import (
	ps "gitlab.com/grpc-first/payment-service/genproto/payment"
)

type StoreStorageI interface {
	CreateCard(req *ps.CreateCardRequest) (*ps.Card, error)
	SubtractBalance(req *ps.UpdateBalanceRequest) (*ps.Card, error)
	AddBalance(req *ps.UpdateBalanceRequest) (*ps.Card, error)
	GetCardInfo(req *ps.Id) (*ps.Card, error)
	DeleteCard(req *ps.Id) (error)
}
