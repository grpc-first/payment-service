package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/grpc-first/payment-service/storage/postgres"
	"gitlab.com/grpc-first/payment-service/storage/repo"
)

type IsStorage interface {
	Store() repo.StoreStorageI
}

type StoragePg struct {
	Db          *sqlx.DB
	ProductRepo repo.StoreStorageI
}

func NewStoragePg(db *sqlx.DB) *StoragePg {
	return &StoragePg{
		Db:          db,
		ProductRepo: postgres.NewPatmentRepo(db),
	}
}

func (s StoragePg) Store() repo.StoreStorageI {
	return s.ProductRepo
}
