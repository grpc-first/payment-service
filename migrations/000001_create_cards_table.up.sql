CREATE TABLE IF NOT EXISTS cards (
    id SERIAL PRIMARY KEY,
    card_number VARCHAR(19) NOT NULL UNIQUE,
    card_type TEXT NOT NULL,
    owner_id INTEGER NOT NULL,
    balance float NOT NULL CHECK(balance >= 0)
);
