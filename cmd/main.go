package main

import (
	"net"

	"gitlab.com/grpc-first/payment-service/config"
	ps "gitlab.com/grpc-first/payment-service/genproto/payment"
	"gitlab.com/grpc-first/payment-service/pkg/db"
	"gitlab.com/grpc-first/payment-service/pkg/logger"
	"gitlab.com/grpc-first/payment-service/service"
	grpcclient "gitlab.com/grpc-first/payment-service/service/grpcClient"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	cfg := config.Load()

	log := logger.New(cfg.LogLevel, "payment-service")
	defer logger.CleanUp(log)

	log.Info("main: sqlx Config",
		logger.String("host", cfg.PostgresHost),
		logger.Int("port", cfg.PostgresPort),
		logger.String("database", cfg.PostgresDatabase),
	)

	connDb, err := db.ConnectToDb(cfg)
	if err != nil {
		log.Fatal("Error while connecting to database: %v", logger.Error(err))
	}
	prClient, err := grpcclient.New(cfg)
	if err != nil {
		log.Fatal("Error while connection to product client", logger.Any("", err))
	}
	paymentService := service.NewPaymentService(prClient, connDb, log)

	lis, err := net.Listen("tcp", cfg.GRPCPort)
	if err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}

	s := grpc.NewServer()
	reflection.Register(s)
	ps.RegisterPaymentServiceServer(s, paymentService)
	log.Info("Server is running", logger.String("Port", cfg.GRPCPort))

	if err := s.Serve(lis); err != nil {
		log.Fatal("Error while listening: %v", logger.Error(err))
	}
}
