package config

import (
	"os"

	"github.com/spf13/cast"
)

type Config struct {
	Environment      string
	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
	LogLevel         string
	GRPCPort         string
	UserServiceHost  string
	UserServicePort  int
	StoreServiceHost string
	StoreServicePort int
}

func Load() Config {
	c := Config{}
	c.Environment = cast.ToString(GetOrReturnDefault("ENVIRONMENT", "develop"))

	c.PostgresHost = cast.ToString(GetOrReturnDefault("POSTGRES_HOST", "localhost"))
	c.PostgresPort = cast.ToInt(GetOrReturnDefault("POSTGRES_PORT", "5432"))
	c.PostgresDatabase = cast.ToString(GetOrReturnDefault("POSTGRES_DATABASE", "payment_service"))
	c.PostgresPassword = cast.ToString(GetOrReturnDefault("POSTGRES_PASSWORD", "Azizbek"))
	c.PostgresUser = cast.ToString(GetOrReturnDefault("POSTGRES_USER", "azizbek"))

	c.UserServiceHost = cast.ToString(GetOrReturnDefault("U_S_HOST", "localhost"))
	c.UserServicePort = cast.ToInt(GetOrReturnDefault("U_S_PORT", 8000))

	c.StoreServiceHost = cast.ToString(GetOrReturnDefault("S_S_HOST", "localhost"))
	c.StoreServicePort = cast.ToInt(GetOrReturnDefault("S_S_PORT", "6000"))

	c.GRPCPort = cast.ToString(GetOrReturnDefault("GRPC_PORT", ":7000"))

	c.LogLevel = cast.ToString(GetOrReturnDefault("LOG_LEVEL", "debug"))

	return c
}

func GetOrReturnDefault(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)
	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
